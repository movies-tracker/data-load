FROM python:3.7

RUN mkdir -p /build

WORKDIR /build

COPY . .

RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "main.py" ]