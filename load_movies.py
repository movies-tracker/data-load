import os
import logging


class LoadMovies:

    def __init__(self, movies_file_path=None):
        super().__init__()
        self.logger = logging.getLogger("data_loader")
        self.movies = []
        movies_file_path = movies_file_path or os.getenv(
            "MOVIES_FILE_PATH") or None
        if not movies_file_path:
            logging.error("No file path defined. Exiting...")
            raise RuntimeError(
                "movies_file_path is undefined. Consider setting MOVIES_FILE_PATH environment variable")
        logging.info(
            "Instantiating movies from path: {}".format(movies_file_path))
        with open(movies_file_path, "r") as movie_file:
            for movie in movie_file.readlines():
                logging.info("Loading movie: {}".format(movie))
                self.movies.append(movie.strip())
