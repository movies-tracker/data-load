import logging
import os
import json


class Translate:

    def __init__(self, dict_path=None):
        super().__init__()
        self.translation_dict = self.__load_translation_dict(dict_path)
        self.logger = logging.getLogger("data_loader")

    def __load_translation_dict(self, dict_path=None):
        path = os.getenv("TRANSLATION_DICT_PATH") or dict_path
        if path is None:
            # Use the default path in root of directory
            path = "transalation_dict.json"
        if not os.path.exists(path):
            raise FileNotFoundError("PATH: {} does not exist".format(path))
        logging.info("Translation dict file path: {}".format(path))
        with open(path, "r") as infile:
            return json.load(infile)

    def translate_record(self, record):
        return self.__translate_request(record)

    def __translate_request(self, request_json):
        """ Converts incoming request keys to translation keys """
        logging.info("Translating id: {}".format(request_json.get("imdbID")))
        result = {}
        for key, val in request_json.items():
            if key in self.translation_dict:
                result[self.translation_dict[key]] = val
            else:
                result[key] = val
        return result
