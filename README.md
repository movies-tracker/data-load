# Data Loader Application

This application is used to load OpenMovies Movie Data into a backend. For now, it supports publishing messages to Apache Kafka
which can be dequeued by any ready consumer

## Usage

1. Through Docker, using the provided Dockerfile

    a. `docker image build -t data-load .` -> Build the image

    b. ```bash
    docker container run --name data-load -e KAFKA_URI=<kafka-connection-string> -e TRANSLATION_DICT_LOC=<full-path-for-translation-dict> data-load
    ```

        i. Alternatively, provide a .env file in the root. Then you don't need the environment variables

    c. Access the data from the Kafka URI

2. As standalone scripts

    a. Export environment variables KAFKA_URI and TRANSLATION_DICT_LOC from your shell

        i. Or provide a .env file in the base directory.

    b. Run `pip install -r requirements.txt` to install the modules. Ideally, done in a virtual environment

    c. Run `python main.py` to run the scripts

    d. Check Apache Kafka

## Rationale

In the process of creating a Movies Tracker application, I found myself seeding the database again and again using a create movies
endpoint. This became very cumbersome so to automate it, I created a simple application that can be run in isolation from the main
application and ensure data is properly loaded.

## Application architecture

The application consumes JSON results from the OMDB API and pushes these into Apache Kafka's queue. Kafka is a pub/sub model system that
decouples two applications. I wanted to let my main application handle the task of creating Movies in the database, so there is a single
point of failure, without putting too much load on the normal processes. So, this application exists to distinguish between the two.
If, in the future, there are more movies to be loaded, this can be easily done by launching this application with a different set of
movies.

### File descriptions

1. Dockerfile -> The Docker image file to build the application as a Docker image

2. `main.py` -> Entry-point for the application

3. `.env` -> The environment variables file. The provided file is just an example, please change the URIs and tokens as needed.

4. `translation_dict.json` -> Translation dictionary JSON file, will map keys to the ones that match your application. Can remove it,
if your application uses the same keys as returned from the API

5. `movies.txt` -> A newline delimited list of movie titles to fetch. Please be aware, this should be less than 1k if not paying
for OMDB API.


## Sharing the data

If you're using Docker, then the data is loaded onto volumes which can be shared across users.