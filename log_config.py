import logging
import os
import sys


def init_logs():
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger("data_loader")
    log_level = os.getenv("LOG_LEVEL")
    if not log_level:
        log_level = logging.INFO
    else:
        if log_level == "ERROR":
            log_level = logging.ERROR
        elif log_level == "DEBUG":
            log_level = logging.DEBUG
        elif log_level == "WARN":
            log_level = logging.WARNING
        else:
            log_level = logging.INFO
    ch = logging.StreamHandler(stream=sys.stdout)
    ch.setLevel(log_level)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
