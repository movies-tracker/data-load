import requests
import os
import logging


class FetchUrl:
    def __init__(self, url):
        self.url = url


class FetchMovie:
    def __init__(self, movies, api_key=None, movies_file_path=None):
        super().__init__()
        self.logger = logging.getLogger("data_loader")
        self.base_url = "https://www.omdbapi.com/"
        self.api_key = api_key or os.getenv("OMDB_API_KEY") or None
        if not self.api_key:
            logging.error("No API key defined. Exiting...")
            raise RuntimeError(
                "api_key is not defined. Consider setting OMDB_API_KEY environment variable")
        self.movies = movies

    def fetch_movie(self, movie):
        if not movie:
            logging.error("No movies defined. Exiting...")
            raise RuntimeError("No movie supplied")
        params = (("apikey", self.api_key), ("t", movie))
        resp = requests.get(self.base_url, params=params)
        if resp.status_code != 200:
            logging.error("Status code: %d Message: %s URL: %s",
                          resp.status_code, resp.json(), resp.url)
            raise RuntimeError("Received not OK from Server")
        logging.info(
            "Calling url: {} with movie: {}".format(resp.url, movie))
        return resp.json()

    def fetch_movies(self):
        if not self.movies:
            logging.error("Movies array not defined. Exiting...")
            raise RuntimeError("No movies defined")
        all_movies_json = []
        for movie in self.movies:
            all_movies_json.append(self.fetch_movie(movie))
        return all_movies_json
