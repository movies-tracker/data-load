from kafka import KafkaProducer
import os
import logging
import json
import pika


class PublishMovie:
    def __init__(self, kafka_uris=None, topic=None):
        super().__init__()
        self.logger = logging.getLogger("data_loader")
        uris = kafka_uris or os.getenv("KAFKA_URI") or None
        if not uris:
            raise RuntimeError(
                "kafka uris not defined. Consider setting KAFKA_URI environment variable")
        self.topic = topic
        if self.topic is None:
            logging.warn("No topic found. Using: %s", topic)
            self.topic = "movies"
        self.producer = KafkaProducer(bootstrap_servers=uris)

    def publish_msg(self, msg):
        logging.info("Publishing message with ID: %s to channel: %s",
                     msg.get("imdbID"), self.topic)
        self.producer.send(bytes(self.topic, "utf-8"),
                           value=json.dumps(msg).encode("utf-8"))


class RabbitPublishMovie:
    def __init__(self, host=None, port=None, queue=None):
        super().__init__()
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        host = host or os.getenv("RABBIT_HOST") or None
        port = port or os.getenv("RABBIT_PORT") or None
        if not host:
            self.logger.error("Missin Rabbit Host")
            raise ValueError("RABBIT_HOST not defined")
        if not port:
            self.logger.warn("No port specified. Using default: %s", "5672")
            port = "5672"
        self.queue_name = queue or os.getenv("RABBIT_QUEUE") or None
        if not self.queue_name:
            self.logger.warn("Missing queuue name. Using: %s", "movies")
            self.queue_name = "movies"
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=host, port=port))
        if not connection:
            self.logger.error(
                "Unable to connect to Host: %s Port: %s", host, port)
            raise ConnectionError("Cannot connect to RabbitMQ")
        # Connect and ensure queue available
        self.producer = connection.channel()
        self.producer.queue_declare(self.queue_name)

    def publish_msg(self, msg):
        self.logger.info("Publishing message with ID: %s to queue: %s", msg.get(
            "imdbID"), self.queue_name)
        self.producer.basic_publish(
            exchange="", routing_key=self.queue_name, body=json.dumps(msg))
