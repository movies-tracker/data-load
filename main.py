import os
import json
from dotenv import load_dotenv
import logging
from publish_movie import RabbitPublishMovie
from fetch_movie import FetchMovie
from translate_movie import Translate
from log_config import init_logs
from load_movies import LoadMovies

load_dotenv()
init_logs()


def main():
    # Load movies
    movies_loader = LoadMovies()
    # Fetch movies
    movies_fetcher = FetchMovie(movies_loader.movies)
    fetched_movies = movies_fetcher.fetch_movies()
    # Translate movies
    movies_translator = Translate()
    for idx, movie in enumerate(fetched_movies):
        fetched_movies[idx] = movies_translator.translate_record(movie)
    # Publish movies
    movies_publisher = RabbitPublishMovie()
    for movie in fetched_movies:
        movies_publisher.publish_msg(movie)
    logger = logging.getLogger("data_loader")
    logger.info("Finished")


if __name__ == "__main__":
    main()
